Welcome to Messenger!
=====================


Voici la procédure à suivre pour installer la machine virtuelle de développement du projet MediaFactory/Messenger

----------


Installation
---------

1. télécharger et installer <a href="https://www.virtualbox.org/" target="_blank">VirtualBox</a>
2. télécharger et installer <a href="http://www.vagrantup.com/" target="_blank">Vagrant</a>
3. lancer la commande "vagrant plugin install vagrant-vbguest"
4. checkout du projet mf_messenger
5. lancer la commande "vagrant up" (depuis le répertoire du projet)

> **NOTE:**
> 
> Quelques erreurs ou avertissements peuvent apparaître lors du premier démarrage de la VM. Veuillez les ignorer. Par exemple :
> ==> default: error:
> ==> default: open of /home/vagrant/messenger/java8.rpm failed: No such file or directory
> ==> default: rm: cannot remove `/usr/bin/jar': No such file or directory
> ==> default: rm:
> ==> default: cannot remove `/var/lib/alternatives/jar'
> ==> default: : No such file or directory

Logiciels installés
---------

Lors du premier lancement de la VM, les logiciels suivants seront installés :

1. wget
2. ftp
3. vi et vim
4. git et gitflow
5. java JDK 8
6. maven
7. ActiveMQ (5.10)

Accès à la VM
---------

Lancer la commande suivante:
> vagrant ssh

Démarrer ActiveMQ
---------
> sudo /opt/apache-activemq-5.10.0/bin/activemq start
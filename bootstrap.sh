#!/usr/bin/env bash

# Basic install
yum clean metadata
yum clean all
yum update -q -y
yum install -q -y wget vi vim ftp git gitflow
rpm -Uvh /home/vagrant/mnt/java8.rpm

# Install Maven
wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo >/dev/null 2>&1
yum install -q -y apache-maven

# Configure JAVA
## java ##
alternatives --install /usr/bin/java java /usr/java/latest/jre/bin/java 200000
## javaws ##
alternatives --install /usr/bin/javaws javaws /usr/java/latest/jre/bin/javaws 200000
 
## Install javac only if you installed JDK (Java Development Kit) package ##
alternatives --install /usr/bin/javac javac /usr/java/latest/bin/javac 200000
rm /usr/bin/jar
rm /var/lib/alternatives/jar
alternatives --install /usr/bin/jar jar /usr/java/latest/bin/jar 200000

su - vagrant
echo 'export JAVA_HOME="/usr/java/latest"' >> /home/vagrant/.bash_profile

# Install ActiveMQ
wget -O /tmp/ http://apache.cs.utah.edu/activemq/5.10.0/apache-activemq-5.10.0-bin.tar.gz
tar zxvf /tmp/apache-activemq-5.10.0-bin.tar.gz
rm /tmp/apache-activemq-5.10.0-bin.tar.gz
mv apache-activemq-5.10.0 /usr/local
chown 'vagrant':vagrant '/etc/default/activemq'
sudo chmod 600 '/etc/default/activemq'
/usr/local/apache-activemq-5.10.0/bin/activemq setup /etc/default/activemq
/usr/local/apache-activemq-5.10.0/bin/activemq start